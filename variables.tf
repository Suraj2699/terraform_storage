variable "sub_id" {
  type = string
}

variable "client_id" {
  type = string
}

variable "client_se" {
  type = string
}

variable "tenant_id" {
  type = string
}

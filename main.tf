provider "azurerm" {
  version = "1.38.0"
  skip_provider_registration = true
  subscription_id = var.sub_id
  client_id = var.client_id
  client_secret = var.client_se
  tenant_id = var.tenant_id
}


resource "azurerm_storage_account" "example" {
  name                     = "surajstorageaccount2699"
  resource_group_name      = "DevOps_Kochi"
  location                 = "eastus"
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "staging"
  }
}
